import React from 'react';
import { NavLink } from 'react-router-dom'

function Navigation(){
    return (
      <nav className="nav-bar">  
        <ul className="nav-bar-list">
          <li>
            <NavLink exact to="/">Home</NavLink>
          </li>
          <li>
            <NavLink exact to="/characters">Characters</NavLink>
          </li>
          <li>
            <NavLink exact to="/locations">Locations</NavLink>
          </li>
        </ul>
      </nav>
    )
  }

  export default Navigation;